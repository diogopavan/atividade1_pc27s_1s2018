/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcpsockets;

/**
 * Exemplo ilustrativo de comunicacao em rede com Sockets Java
 *
 * Autor: Diogo Reis Pavan 
 * Ultima modificacao: 28/03/2018 23:25
 *
 */
import java.io.*;
import java.net.*;
import java.util.Random;

class TCPClient {

    public TCPClient() {
        super();

        try {
            String sentence;
            String modifiedSentence;
            
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
            Socket clientSocket = new Socket("localhost", 6789);
            sentence = inFromUser.readLine();
            
            for (int i = 0; i < 1000; i++) {
                int resposta = setTimeOut();
                DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
                outToServer.writeBytes(sentence + '\n');

                if (resposta > 500) {
                    System.out.println("Acabou seu tempo");
                } else {
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    modifiedSentence = inFromServer.readLine();
                    System.out.println("FROM SERVER: " + modifiedSentence);
                }
            }
            clientSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void main(String argv[]) throws Exception {
        TCPClient tcpClient = new TCPClient();
    }

    public int setTimeOut() {

        int tempoTimeOut = 0;

        try {
            Thread timeout = new Thread();
            Random tempo = new Random();
            
            tempoTimeOut = tempo.nextInt(1000); 
            System.out.println("Tempo do timeout: " + tempoTimeOut);
            
            timeout.sleep(tempoTimeOut);
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

        return tempoTimeOut;
    }
}
