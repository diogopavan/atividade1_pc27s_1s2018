package udpsockets;

/**
 * Exemplo ilustrativo de comunicacao em rede com Sockets Java
 *
 * Autor: Diogo Reis Pavan
 * Ultima modificacao: 14/03/2018 23:29
 *
 */
import java.io.*;
import java.net.*;

class UDPServer {

    public static void main(String args[]) throws Exception {
        
        //Cria o socket servidor e determina a porta que o servidor escuta
        DatagramSocket serverSocket = new DatagramSocket(9876);
        byte[] receiveData = new byte[1024];
        byte[] sendData = new byte[1024];
        while (true) {
            
            //Cria objeto de datagrama para receber outro pacote
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);

            String sentence = new String(receivePacket.getData());
            System.out.println("RECEIVED: " + sentence);
            
            //Note que os dados para resposta ao cliente sao
            //coletados do pacote recebido
            //Busca o IP e a PORTA no pacote que recebeu
            InetAddress IPAddress = receivePacket.getAddress();
            int port = receivePacket.getPort();

            //Prepara a resposta
            String capitalizedSentence = sentence.toUpperCase();
            sendData = capitalizedSentence.getBytes();

            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
            serverSocket.send(sendPacket);
        }
    }
}
