package udpsockets;

/**
 * Exemplo ilustrativo de comunicacao em rede com Sockets Java
 *
 * Autor: Diogo Reis Pavan 
 * Ultima modificacao: 28/03/2018 23:25
 *
 */
import java.io.*;
import java.net.*;
import java.util.Random;

class UDPClient {

    public UDPClient() {
        super();

        try {
            BufferedReader inFromUser
                    = new BufferedReader(new InputStreamReader(System.in));
            /**
             * DatagramSocket cria um socket (canal de comunicação que tem os
             * parâmetros IP e PORTA) O objeto da classe ele possui operações de
             * SEND (enviar) e RECEIVE (receber) datagramas
             */
            DatagramSocket clientSocket = new DatagramSocket();
            InetAddress IPAddress = InetAddress.getByName("localhost");

            //UDP tem tamanho fixo, envia cada pacote com tamanho fixo
            byte[] sendData = new byte[1024];
            byte[] receiveData = new byte[1024];

            String sentence = inFromUser.readLine();
            //Datagrama deve ser em bytes, a linha seguinte converte
            sendData = sentence.getBytes();

            //Dados do cliente estao inteiramente contidos no pacote
            //Servidor usarah os dados do pacote para responder no socket do cliente
            for (int i = 0; i < 1000; i++) {
                /**
                 * Pacote com informações convertidas para bytes são mandados
                 * pelo socket Que é criado pelo DatagramPacket
                 */
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 9876);
                clientSocket.send(sendPacket);

                try {
                    int resposta = setTimeOut();

                    if (resposta > 500) {
                        System.out.println("Acabou seu tempo");
                    } else {
                        //DatagramPacket cria o pacote para receber
                        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                        clientSocket.receive(receivePacket); //note que a recepção eh feita no socket cliente

                        String modifiedSentence = new String(receivePacket.getData());
                        System.out.println("FROM SERVER:" + modifiedSentence);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println(e.getMessage());
                }
            }

            //Fechar o socket
            clientSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void main(String args[]) throws Exception {
        UDPClient udpClient = new UDPClient();
    }

    public int setTimeOut() {

        int tempoTimeOut = 0;

        try {
            Thread timeout = new Thread();
            Random tempo = new Random();
            
            tempoTimeOut = tempo.nextInt(1000);            
            System.out.println("Tempo do timeout: " + tempoTimeOut);
         
            timeout.sleep(tempoTimeOut);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

        return tempoTimeOut;
    }
}
